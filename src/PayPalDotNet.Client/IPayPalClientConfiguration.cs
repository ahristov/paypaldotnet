namespace PayPalDotNet.Client
{
    public interface IPayPalClientConfiguration
    {
        string ClientId { get; }
        string ClientSecret { get; }
        string PayPalApiUrl { get; }
    }
}