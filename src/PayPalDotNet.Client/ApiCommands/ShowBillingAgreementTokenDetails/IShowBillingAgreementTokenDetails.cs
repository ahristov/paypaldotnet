using System.Threading.Tasks;

namespace PayPalDotNet.Client.ApiCommands.ShowBillingAgreementTokenDetails
{
    public interface IShowBillingAgreementTokenDetails
    {
        Task<ShowBillingAgreementTokenDetailsResponse> ShowBillingAgreementTokenAsync(string ba_token);
        ShowBillingAgreementTokenDetailsResponse ShowBillingAgreementToken(string ba_token);
    }
}