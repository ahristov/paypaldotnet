using Newtonsoft.Json;
using PayPalDotNet.Client.DataContracts;
using System;
using System.Collections.Generic;

namespace PayPalDotNet.Client.ApiCommands.ShowBillingAgreementTokenDetails
{
    [Serializable]
    public class ShowBillingAgreementTokenDetailsResponse : IDto
    {
        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("token_id")]
        public string TokenId { get; set; }

        [JsonProperty("token_status")]
        public string TokenStatus { get; set; }

        [JsonProperty("skip_shipping_address")]
        public bool SkipShippingAddress { get; set; }

        [JsonProperty("immutable_shipping_address")]
        public bool ImmutableShippingAddress { get; set; }

        [JsonProperty("redirect_urls")]
        public RedirectUrls RedirectUrls { get; set; }

        [JsonProperty("plan_unit_list")]
        public IEnumerable<PlanUnitList> PlanUnitList { get; set; }

        [JsonProperty("payer_info")]
        public PayerInfo PayerInfo { get; set; }

        [JsonProperty("owner")]
        public Owner Owner { get; set; }

        [JsonIgnore]
        public bool IsApproved => this.TokenStatus == "APPROVED";

        public ShowBillingAgreementTokenDetailsResponse()
        {
            this.RedirectUrls = new RedirectUrls();
            this.PlanUnitList = new List<PlanUnitList>();
            this.PayerInfo = new PayerInfo();
            this.Owner = new Owner();
        }
    }

}