using System.Threading.Tasks;

namespace PayPalDotNet.Client.ApiCommands.PaymentWithReferenceTransaction
{
    public interface IPaymentWithReferenceTransaction
    {
        Task<PaymentWithReferenceTransactionResponse> PaymentWithReferenceTransactionAsync(PaymentWithReferenceTransactionRequest paymentRequest);
        PaymentWithReferenceTransactionResponse PaymentWithReferenceTransaction(PaymentWithReferenceTransactionRequest paymentRequest);
    }
}