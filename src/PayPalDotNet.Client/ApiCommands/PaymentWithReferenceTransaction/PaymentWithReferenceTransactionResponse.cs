using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using PayPalDotNet.Client.DataContracts;

namespace PayPalDotNet.Client.ApiCommands.PaymentWithReferenceTransaction
{
    [Serializable]
    public class PaymentWithReferenceTransactionResponse : IDto
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("intent")]
        public string Intent { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("payer")]
        public PayerFromResponse Payer { get; set; }

        [JsonProperty("transactions")]
        public IEnumerable<TransactionFromResponse> Transactions { get; set; }

        [JsonProperty("redirect_urls")]
        public RedirectUrls RedirectUrls { get; set; }

        [JsonProperty("create_time")]
        public DateTimeOffset CreateTime { get; set; }

        [JsonProperty("update_time")]
        public DateTimeOffset UpdateTime { get; set; }

        [JsonProperty("links")]
        public IEnumerable<Link> Links { get; set; }

        [JsonIgnore]
        public bool IsApproved => this.State == "approved";

        [JsonIgnore]
        public string SaleId => this.Transactions?.FirstOrDefault()?.RelatedResources?.FirstOrDefault().Sale?.Id;

        public PaymentWithReferenceTransactionResponse()
        {
            this.Payer = new DataContracts.PayerFromResponse();
            this.Transactions = new List<TransactionFromResponse>();
            this.RedirectUrls = new RedirectUrls();
            this.Links = new List<Link>();
        }
    }

}