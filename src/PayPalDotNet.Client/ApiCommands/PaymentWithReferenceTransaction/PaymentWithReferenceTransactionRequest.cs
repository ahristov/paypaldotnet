using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using PayPalDotNet.Client.DataContracts;

namespace PayPalDotNet.Client.ApiCommands.PaymentWithReferenceTransaction
{
    [Serializable]
    public class PaymentWithReferenceTransactionRequest : IDto
    {
        [JsonProperty("intent")]
        public string Intent { get; set; }

        [JsonProperty("payer")]
        public Payer Payer { get; set; }

        [JsonProperty("transactions")]
        public IEnumerable<Transaction> Transactions { get; set; }

        [JsonProperty("redirect_urls")]
        public RedirectUrls RedirectUrls { get; set; }

        public PaymentWithReferenceTransactionRequest()
        {
            this.Intent = "sale";
            this.Payer = new Payer();
            this.Transactions = new List<Transaction>();
            this.RedirectUrls = new RedirectUrls();
        }
    }
}