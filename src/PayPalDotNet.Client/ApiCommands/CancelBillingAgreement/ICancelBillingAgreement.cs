﻿using PayPalDotNet.Client.DataContracts;

namespace PayPalDotNet.Client.ApiCommands.CancelBillingAgreement
{
    public interface ICancelBillingAgreement
    {
        void CancelBillingAgreement(string billing_agreement);
        void CancelBillingAgreement(string billing_agreement, string note);
    }
}
