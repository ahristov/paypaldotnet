﻿using System.Threading.Tasks;

namespace PayPalDotNet.Client.ApiCommands.ShowRefund
{
    public interface IShowRefund
    {
        Task<ShowRefundResponse> ShowRefundAsync(string refund_id);
        ShowRefundResponse ShowRefund(string refund_id);
    }
}
