using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using PayPalDotNet.Client.DataContracts;

namespace PayPalDotNet.Client.ApiCommands.CreateBillingAgreementToken
{
    [Serializable]
    public class CreateBillingAgreementTokenResponse : IDto
    {
        const string APPROVAL_URL = "approval_url";

        [JsonProperty("links")]
        public IEnumerable<Link> Links { get; private set; }

        [JsonProperty("token_id")]
        public string TokenId { get; set; }

        [JsonIgnore]
        public string ApprovalUrl
        {
            get
            {
                foreach (var link in this.Links ?? new List<Link>())
                {
                    if (link.Rel == APPROVAL_URL)
                    {
                        return link.Href;
                    }
                }

                return null;
            }
        }

        public CreateBillingAgreementTokenResponse()
        {
            this.Links = new List<Link>();
        }

    }

}