using System;
using Newtonsoft.Json;
using PayPalDotNet.Client.DataContracts;

namespace PayPalDotNet.Client.ApiCommands.CreateBillingAgreementToken
{
    [Serializable]
    public class CreateBillingAgreementTokenRequest : IDto
    {
        [JsonProperty("description")]
        public string Description { get; private set; }

        [JsonProperty("payer")]
        public Payer Payer { get; set; }

        [JsonProperty("plan")]
        public Plan Plan { get; set; }

        public CreateBillingAgreementTokenRequest()
        {
            this.Description = "Billing Agreement";
            this.Payer = Payer.CreatePayerWithPayPalPaymentMethod();
            this.Plan = Plan.CreatePlanForMerchantInitiatedBillingWithInstantPayments();
        }

        public CreateBillingAgreementTokenRequest(string returnUrl, string cancelUrl)
            : this()
        {
            this.Plan.MerchantPreferences.ReturnUrl = returnUrl;
            this.Plan.MerchantPreferences.CancelUrl = cancelUrl;
        }
    }
}