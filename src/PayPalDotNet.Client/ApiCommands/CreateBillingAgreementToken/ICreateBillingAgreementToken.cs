using System.Threading.Tasks;

namespace PayPalDotNet.Client.ApiCommands.CreateBillingAgreementToken
{
    public interface ICreateBillingAgreementToken
    {
        Task<CreateBillingAgreementTokenResponse> CreateBillingAgreementTokenAsync(CreateBillingAgreementTokenRequest baTokenRequest);
        CreateBillingAgreementTokenResponse CreateBillingAgreementToken(CreateBillingAgreementTokenRequest baTokenRequest);
    }
}