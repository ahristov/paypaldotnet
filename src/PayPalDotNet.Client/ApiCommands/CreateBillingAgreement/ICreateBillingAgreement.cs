using System.Threading.Tasks;

namespace PayPalDotNet.Client.ApiCommands.CreateBillingAgreement
{
    public interface ICreateBillingAgreement
    {
        Task<CreateBillingAgreementResponse> CreateBillingAgreementAsync(string token_id);
        CreateBillingAgreementResponse CreateBillingAgreement(string token_id);
    }
}