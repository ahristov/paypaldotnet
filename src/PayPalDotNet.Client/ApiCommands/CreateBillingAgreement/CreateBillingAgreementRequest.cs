using Newtonsoft.Json;

namespace PayPalDotNet.Client.ApiCommands.CreateBillingAgreement
{
    public class CreateBillingAgreementRequest : IDto
    {
        [JsonProperty("token_id")]
        public string TokenId { get; set; }
    }
}