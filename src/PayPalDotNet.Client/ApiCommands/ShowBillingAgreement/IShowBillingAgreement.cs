using System.Threading.Tasks;

namespace PayPalDotNet.Client.ApiCommands.ShowBillingAgreement
{
    public interface IShowBillingAgreement
    {
        Task<ShowBillingAgreementResponse> ShowBillingAgreementAsync(string billing_agreement);
        ShowBillingAgreementResponse ShowBillingAgreement(string billing_agreement);
    }
}