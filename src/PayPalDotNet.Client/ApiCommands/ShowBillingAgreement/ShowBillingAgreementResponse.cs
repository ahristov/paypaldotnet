using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using PayPalDotNet.Client.DataContracts;

namespace PayPalDotNet.Client.ApiCommands.ShowBillingAgreement
{
    [Serializable]
    public class ShowBillingAgreementResponse : IDto
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("payer")]
        public PayerFromResponse Payer { get; set; }

        [JsonProperty("plan")]
        public Plan Plan { get; set; }

        [JsonProperty("links")]
        public IEnumerable<Link> Links { get; set; }

        [JsonProperty("merchant")]
        public Merchant Merchant { get; set; }

        [JsonProperty("create_time")]
        public DateTimeOffset CreateTime { get; set; }

        [JsonProperty("update_time")]
        public DateTimeOffset UpdateTime { get; set; }

        [JsonIgnore]
        public bool IsActive => this.State == "ACTIVE";

        [JsonIgnore]
        public bool IsInactive => this.State == "INACTIVE";


        public ShowBillingAgreementResponse()
        {
            this.Payer = new PayerFromResponse();
            this.Plan = new Plan();
            this.Links = new List<Link>();
            this.Merchant = new Merchant();
        }
    }
}