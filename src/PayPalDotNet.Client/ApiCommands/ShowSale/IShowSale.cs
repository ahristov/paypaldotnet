﻿using System.Threading.Tasks;

namespace PayPalDotNet.Client.ApiCommands.ShowSale
{
    public interface IShowSale
    {
        Task<ShowSaleResponse> ShowSaleAsync(string sale_id);
        ShowSaleResponse ShowSale(string sale_id);
    }
}
