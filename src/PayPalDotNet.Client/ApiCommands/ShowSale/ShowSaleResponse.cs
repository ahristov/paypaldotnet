﻿using Newtonsoft.Json;
using PayPalDotNet.Client.DataContracts;
using System;
using System.Collections.Generic;

namespace PayPalDotNet.Client.ApiCommands.ShowSale
{
    public class ShowSaleResponse : IDto
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("amount")]
        public Amount Amount { get; set; }

        [JsonProperty("payment_mode")]
        public string PaymentMode { get; set; }

        [JsonProperty("protection_eligibility")]
        public string ProtectionEligibility { get; set; }

        [JsonProperty("protection_eligibility_type")]
        public string ProtectionEligibilityType { get; set; }

        [JsonProperty("transaction_fee")]
        public TransactionAmount TransactionFee { get; set; }

        [JsonProperty("invoice_number")]
        public string InvoiceNumber { get; set; }

        [JsonProperty("billing_agreement_id")]
        public string BillingAgreementId { get; set; }

        [JsonProperty("parent_payment")]
        public string ParentPayment { get; set; }

        [JsonProperty("create_time")]
        public DateTimeOffset CreateTime { get; set; }

        [JsonProperty("update_time")]
        public DateTimeOffset UpdateTime { get; set; }

        [JsonProperty("links")]
        public IEnumerable<Link> Links { get; set; }

        [JsonIgnore]
        public bool IsCompleted => this.State == "completed";

        [JsonIgnore]
        public bool IsPartiallyRefunded => this.State == "partially_refunded";

        [JsonIgnore]
        public bool IsFullyRefunded => this.State == "refunded";

        public ShowSaleResponse()
        {
            this.Amount = new Amount();
            this.TransactionFee = new TransactionAmount();
            this.InvoiceNumber = Guid.NewGuid().ToString();
            this.Links = new List<Link>();
        }
    }
}
