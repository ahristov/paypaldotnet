using System;
using Newtonsoft.Json;

namespace PayPalDotNet.Client.ApiCommands.GetAssetToken
{
    [Serializable]
    internal class GetAccessTokenRequest
    {
        [JsonProperty("clientId")]
        public string ClientId { get; set; }

        [JsonProperty("clientSecret")]
        public string ClientSecret { get; set; }
    }
}