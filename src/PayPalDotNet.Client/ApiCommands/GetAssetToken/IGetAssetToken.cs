using System.Threading.Tasks;

namespace PayPalDotNet.Client.ApiCommands.GetAssetToken
{
    /// <summary>
    /// Get access token
    /// </summary>
    internal interface IGetAssetToken
    {
        Task<GetAccessTokenResponse> GetAccessTokenAsync(GetAccessTokenRequest request);
        GetAccessTokenResponse GetAccessToken(GetAccessTokenRequest request);
    }
}