﻿using Newtonsoft.Json;
using PayPalDotNet.Client.DataContracts;
using System;

namespace PayPalDotNet.Client.ApiCommands.RefundSale
{
    [Serializable]
    public class RefundSaleRequest : IDto
    {
        [JsonProperty("amount")]
        public Amount Amount { get; set; }

        [JsonProperty("invoice_number")]
        public string InvoiceNumber { get; set; }

        public RefundSaleRequest()
        {
            this.Amount = new Amount();
        }
    }
}
