﻿using Newtonsoft.Json;
using PayPalDotNet.Client.DataContracts;
using System;
using System.Collections.Generic;

namespace PayPalDotNet.Client.ApiCommands.RefundSale
{
    [Serializable]
    public class RefundSaleResponse : IDto
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("create_time")]
        public DateTimeOffset CreateTime { get; set; }

        [JsonProperty("update_time")]
        public DateTimeOffset UpdateTime { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("amount")]
        public Amount Amount { get; set; }

        [JsonProperty("refund_from_transaction_fee")]
        public TransactionAmount RefundFromTransactionFee { get; set; }

        [JsonProperty("total_refunded_amount")]
        public TransactionAmount TotalRefundedAmount { get; set; }

        [JsonProperty("refund_from_received_amount")]
        public TransactionAmount RefundFromReceivedAmount { get; set; }

        [JsonProperty("sale_id")]
        public string SaleId { get; set; }

        [JsonProperty("parent_payment")]
        public string ParentPayment { get; set; }

        [JsonProperty("invoice_number")]
        public string InvoiceNumber { get; set; }

        [JsonProperty("links")]
        public IEnumerable<Link> Links { get; set; }

        [JsonIgnore]
        public bool IsCompleted => this.State == "completed";

        public RefundSaleResponse()
        {
            this.Amount = new Amount();
            this.RefundFromTransactionFee = new TransactionAmount();
            this.TotalRefundedAmount = new TransactionAmount();
            this.RefundFromReceivedAmount = new TransactionAmount();
            this.Links = new List<Link>();
        }

    }
}
