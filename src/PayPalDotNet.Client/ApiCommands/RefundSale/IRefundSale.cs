﻿using System.Threading.Tasks;

namespace PayPalDotNet.Client.ApiCommands.RefundSale
{
    public interface IRefundSale
    {
        Task<RefundSaleResponse> RefundSaleAsync(string sale_id, RefundSaleRequest refundRequest);
        RefundSaleResponse RefundSale(string sale_id, RefundSaleRequest refundRequest);
    }
}
