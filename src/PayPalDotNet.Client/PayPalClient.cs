using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using PayPalDotNet.Client.ApiCommands.CreateBillingAgreement;
using PayPalDotNet.Client.ApiCommands.CreateBillingAgreementToken;
using PayPalDotNet.Client.ApiCommands.GetAssetToken;
using PayPalDotNet.Client.ApiCommands.PaymentWithReferenceTransaction;
using PayPalDotNet.Client.ApiCommands.RefundSale;
using PayPalDotNet.Client.ApiCommands.ShowBillingAgreement;
using PayPalDotNet.Client.ApiCommands.ShowBillingAgreementTokenDetails;
using PayPalDotNet.Client.ApiCommands.ShowRefund;
using PayPalDotNet.Client.ApiCommands.ShowSale;
using PayPalDotNet.Client.DataContracts;

namespace PayPalDotNet.Client
{
    public class PayPalClient : IPayPalClient
    {
        private readonly IPayPalClientConfiguration _config;
        private readonly HttpClient _httpClient;

        public PayPalClient(IPayPalClientConfiguration config, HttpClient httpClient)
        {
            _config = config ?? throw new ArgumentNullException(nameof(config));
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));

            _httpClient.BaseAddress = new Uri(_config.PayPalApiUrl);
            _httpClient.DefaultRequestHeaders
              .Accept
              .Add(new MediaTypeWithQualityHeaderValue("*/*"));
        }

        public async Task<CreateBillingAgreementTokenResponse> CreateBillingAgreementTokenAsync(CreateBillingAgreementTokenRequest baTokenRequest)
            => await RunApiCall<CreateBillingAgreementTokenResponse>(HttpMethod.Post, ApiPaths.CreateBillingAgreementToken, baTokenRequest);
        public CreateBillingAgreementTokenResponse CreateBillingAgreementToken(CreateBillingAgreementTokenRequest baTokenRequest)
            => CreateBillingAgreementTokenAsync(baTokenRequest).GetAwaiter().GetResult();

        public async Task<ShowBillingAgreementTokenDetailsResponse> ShowBillingAgreementTokenAsync(string ba_token)
            => await RunApiCall<ShowBillingAgreementTokenDetailsResponse>(HttpMethod.Get, ApiPaths.ShowBillingAgreementTokenDetails.Replace("{{ba_token}}", Tools.UrlEncode(ba_token)));
        public ShowBillingAgreementTokenDetailsResponse ShowBillingAgreementToken(string ba_token)
            => ShowBillingAgreementTokenAsync(ba_token).GetAwaiter().GetResult();

        public async Task<CreateBillingAgreementResponse> CreateBillingAgreementAsync(string token_id)
            => await RunApiCall<CreateBillingAgreementResponse>(HttpMethod.Post, ApiPaths.CreateBillingAgreement, new CreateBillingAgreementRequest { TokenId = token_id });
        public CreateBillingAgreementResponse CreateBillingAgreement(string token_id)
            => CreateBillingAgreementAsync(token_id).GetAwaiter().GetResult();

        public async Task<ShowBillingAgreementResponse> ShowBillingAgreementAsync(string billing_agreement)
            => await RunApiCall<ShowBillingAgreementResponse>(HttpMethod.Get, ApiPaths.ShowBillingAgreement.Replace("{{billing_agreement}}", Tools.UrlEncode(billing_agreement)));
        public ShowBillingAgreementResponse ShowBillingAgreement(string billing_agreement)
            => ShowBillingAgreementAsync(billing_agreement).GetAwaiter().GetResult();

        public async Task<PaymentWithReferenceTransactionResponse> PaymentWithReferenceTransactionAsync(PaymentWithReferenceTransactionRequest paymentRequest)
            => await RunApiCall<PaymentWithReferenceTransactionResponse>(HttpMethod.Post, ApiPaths.PaymentWithReferenceTransaction, paymentRequest);
        public PaymentWithReferenceTransactionResponse PaymentWithReferenceTransaction(PaymentWithReferenceTransactionRequest paymentRequest)
            => PaymentWithReferenceTransactionAsync(paymentRequest).GetAwaiter().GetResult();

        public async Task<ShowSaleResponse> ShowSaleAsync(string sale_id)
            => await RunApiCall<ShowSaleResponse>(HttpMethod.Get, ApiPaths.ShowSale.Replace("{{sale_id}}", Tools.UrlEncode(sale_id)));
        public ShowSaleResponse ShowSale(string sale_id)
            => ShowSaleAsync(sale_id).GetAwaiter().GetResult();

        public async Task<RefundSaleResponse> RefundSaleAsync(string sale_id, RefundSaleRequest refundRequest)
            => await RunApiCall<RefundSaleResponse>(HttpMethod.Post, ApiPaths.RefundSale.Replace("{{sale_id}}", Tools.UrlEncode(sale_id)), refundRequest);
        public RefundSaleResponse RefundSale(string sale_id, RefundSaleRequest refundRequest)
            => RefundSaleAsync(sale_id, refundRequest).GetAwaiter().GetResult();

        public async Task<ShowRefundResponse> ShowRefundAsync(string refund_id)
            => await RunApiCall<ShowRefundResponse>(HttpMethod.Get, ApiPaths.ShowRefund.Replace("{{refund_id}}", Tools.UrlEncode(refund_id)));
        public ShowRefundResponse ShowRefund(string refund_id)
            => ShowRefundAsync(refund_id).GetAwaiter().GetResult();

        public void CancelBillingAgreement(string billing_agreement)
            => CancelBillingAgreement(billing_agreement, string.Empty);

        public void CancelBillingAgreement(string billing_agreement, string note)
            => RunApiCall<NoContentResponse>(HttpMethod.Post, ApiPaths.CancelBillingAgreement.Replace("{{billing_agreement}}", Tools.UrlEncode(billing_agreement)), new AgreementState { Note = note }).GetAwaiter().GetResult();

        //

        private async Task<GetAccessTokenResponse> GetAccessToken()
          => await GetAccessToken(new GetAccessTokenRequest
          {
              ClientId = _config.ClientId,
              ClientSecret = _config.ClientSecret
          });

        private async Task<GetAccessTokenResponse> GetAccessToken(GetAccessTokenRequest request)
        {
            var httpRequest = new HttpRequestMessage(HttpMethod.Post, ApiPaths.GetAssetToken);

            var content = new FormUrlEncodedContent(new Dictionary<string, string>
            {
                {"grant_type", "client_credentials"}
            });

            httpRequest.Content = content;

            var credentials = Tools.Base64Encode($"{_config.ClientId}:{_config.ClientSecret}");
            httpRequest.Headers.Authorization = new AuthenticationHeaderValue("Basic", credentials);

            var response = await _httpClient.SendAsync(httpRequest).ConfigureAwait(false);
            var output = await response.Content.ReadAsStringAsync();

            return Tools.FromJson<GetAccessTokenResponse>(output);
        }

        private async Task<TResponse> RunApiCall<TResponse>(HttpMethod httpMethod, string apiPath, IDto request = null) where TResponse : IDto
        {
            var accessToken = GetAccessToken().GetAwaiter().GetResult();

            var httpRequest = new HttpRequestMessage(httpMethod, apiPath);
            httpRequest.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken.AccessToken);

            if (request != null)
            {
                httpRequest.Content = request.ToHttpContent();
            }

            using (var response = await _httpClient.SendAsync(httpRequest).ConfigureAwait(false))
            {
                var output = await response.Content.ReadAsStringAsync();
                return Tools.FromJson<TResponse>(output);
            }
        }

    }
}