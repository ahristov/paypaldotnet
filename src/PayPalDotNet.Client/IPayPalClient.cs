namespace PayPalDotNet.Client
{
    public interface IPayPalClient
        :
        ApiCommands.CreateBillingAgreementToken.ICreateBillingAgreementToken,
        ApiCommands.ShowBillingAgreementTokenDetails.IShowBillingAgreementTokenDetails,
        ApiCommands.CreateBillingAgreement.ICreateBillingAgreement,
        ApiCommands.ShowBillingAgreement.IShowBillingAgreement,
        ApiCommands.PaymentWithReferenceTransaction.IPaymentWithReferenceTransaction,
        ApiCommands.ShowSale.IShowSale,
        ApiCommands.RefundSale.IRefundSale,
        ApiCommands.ShowRefund.IShowRefund,
        ApiCommands.CancelBillingAgreement.ICancelBillingAgreement
    { }
}