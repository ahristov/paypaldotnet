﻿using System;

namespace PayPalDotNet.Client
{
    [Serializable]
    public class NoContentResponse : IDto
    {
    }
}
