﻿namespace PayPalDotNet.Client
{
    internal static class ApiPaths
    {
        internal static string GetAssetToken => "v1/oauth2/token";
        internal static string CreateBillingAgreementToken => "v1/billing-agreements/agreement-tokens";
        internal static string ShowBillingAgreementTokenDetails => "v1/billing-agreements/agreement-tokens/{{ba_token}}";
        internal static string CreateBillingAgreement => "v1/billing-agreements/agreements";
        internal static string ShowBillingAgreement => "v1/billing-agreements/agreements/{{billing_agreement}}";
        internal static string PaymentWithReferenceTransaction => "v1/payments/payment";
        internal static string ShowSale => "v1/payments/sale/{{sale_id}}";
        internal static string RefundSale => "v1/payments/sale/{{sale_id}}/refund";
        internal static string ShowRefund => "v1/payments/refund/{{refund_id}}";
        internal static string CancelBillingAgreement => "v1/billing-agreements/agreements/{{billing_agreement}}/cancel";
        internal static string SearchTransactions => "v1/reporting/transactions?fields=all&start_date={{start_date}}&end_date={{end_date}}";
    }
}
