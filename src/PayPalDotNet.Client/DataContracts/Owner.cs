﻿using Newtonsoft.Json;
using System;

namespace PayPalDotNet.Client.DataContracts
{
    [Serializable]
    public class Owner
    {
        [JsonProperty("merchant_id")]
        public string MerchantId { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }
    }
}
