using System;
using Newtonsoft.Json;

namespace PayPalDotNet.Client.DataContracts
{
    [Serializable]
    public partial class Transaction
    {
        [JsonProperty("amount")]
        public Amount Amount { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("custom")]
        public string Custom { get; set; }

        [JsonProperty("note_to_payee")]
        public string NoteToPayee { get; set; }

        [JsonProperty("invoice_number")]
        public string InvoiceNumber { get; set; }

        [JsonProperty("item_list")]
        public ItemList ItemList { get; set; }

        public Transaction()
        {
            this.Amount = new Amount();
            this.ItemList = new ItemList();
        }
    }
}