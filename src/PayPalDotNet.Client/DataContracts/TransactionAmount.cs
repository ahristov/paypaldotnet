using System;
using Newtonsoft.Json;

namespace PayPalDotNet.Client.DataContracts
{
    [Serializable]
    public partial class TransactionAmount
    {
        [JsonProperty("value")]
        [JsonConverter(typeof(ParseDecimalStringConverter))]
        public decimal Value { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }
    }
}