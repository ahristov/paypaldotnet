using System;
using Newtonsoft.Json;

namespace PayPalDotNet.Client.DataContracts
{
    [Serializable]
    public partial class RelatedResource
    {
        [JsonProperty("sale")]
        public Sale Sale { get; set; }

        public RelatedResource()
        {
            Sale = new Sale();
        }
    }
}