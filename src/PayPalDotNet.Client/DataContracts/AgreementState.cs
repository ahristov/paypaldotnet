﻿using Newtonsoft.Json;
using System;

namespace PayPalDotNet.Client.DataContracts
{
    [Serializable]
    public class AgreementState : IDto
    {
        [JsonProperty("note")]
        public string Note { get; set; }
    }
}
