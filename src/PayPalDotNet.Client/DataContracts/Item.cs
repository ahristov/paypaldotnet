using System;
using Newtonsoft.Json;

namespace PayPalDotNet.Client.DataContracts
{
    [Serializable]
    public partial class Item
    {
        [JsonProperty("sku")]
        public string Sku { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("quantity")]
        [JsonConverter(typeof(ParseIntStringConverter))]
        public int Quantity { get; set; }

        [JsonProperty("price")]
        [JsonConverter(typeof(ParseDecimalStringConverter))]
        public decimal Price { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("tax")]
        [JsonConverter(typeof(ParseDecimalStringConverter))]
        public decimal Tax { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }
    }
}