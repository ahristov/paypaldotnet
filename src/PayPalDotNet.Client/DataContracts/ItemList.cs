using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace PayPalDotNet.Client.DataContracts
{
    [Serializable]
    public partial class ItemList
    {
        [JsonProperty("items")]
        public IEnumerable<Item> Items { get; set; }

        public ItemList()
        {
            this.Items = new List<Item>();
        }
    }
}