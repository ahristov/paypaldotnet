using System;
using Newtonsoft.Json;

namespace PayPalDotNet.Client.DataContracts
{
    [Serializable]
    public partial class FundingInstrument
    {
        [JsonProperty("billing")]
        public Billing Billing { get; set; }

        public FundingInstrument()
        {
            this.Billing = new Billing();
        }
    }
}