using System;
using Newtonsoft.Json;

namespace PayPalDotNet.Client.DataContracts
{
    [Serializable]
    public partial class AmountDetails
    {
        [JsonProperty("subtotal")]
        [JsonConverter(typeof(ParseDecimalStringConverter))]
        public decimal Subtotal { get; set; }

        [JsonProperty("tax")]
        [JsonConverter(typeof(ParseDecimalStringConverter))]
        public decimal Tax { get; set; }

        [JsonProperty("shipping")]
        [JsonConverter(typeof(ParseDecimalStringConverter))]
        public decimal Shipping { get; set; }

        [JsonProperty("insurance")]
        [JsonConverter(typeof(ParseDecimalStringConverter))]
        public decimal Insurance { get; set; }

        [JsonProperty("handling_fee")]
        [JsonConverter(typeof(ParseDecimalStringConverter))]
        public decimal HandlingFee { get; set; }

        [JsonProperty("shipping_discount")]
        [JsonConverter(typeof(ParseDecimalStringConverter))]
        public decimal ShippingDiscount { get; set; }
    }
}