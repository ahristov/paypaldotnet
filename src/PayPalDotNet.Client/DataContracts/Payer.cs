using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace PayPalDotNet.Client.DataContracts
{
    [Serializable]
    public partial class Payer
    {
        [JsonProperty("payment_method")]
        public string PaymentMethod { get; set; }

        [JsonProperty("funding_instruments")]
        public IEnumerable<FundingInstrument> FundingInstruments { get; set; }

        public Payer()
        {
            this.FundingInstruments = new List<FundingInstrument>();
        }

        public static Payer CreatePayerWithPayPalPaymentMethod()
        {
            var res = new Payer();
            res.PaymentMethod = "PAYPAL";
            return res;
        }
    }
}