﻿using Newtonsoft.Json;
using System;

namespace PayPalDotNet.Client.DataContracts
{
    [Serializable]
    public class Merchant
    {
        [JsonProperty("payee_info")]
        public Payee PayeeInfo { get; set; }

        public Merchant()
        {
            this.PayeeInfo = new Payee();
        }
    }
}
