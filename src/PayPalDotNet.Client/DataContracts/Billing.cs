using System;
using Newtonsoft.Json;

namespace PayPalDotNet.Client.DataContracts
{
    [Serializable]
    public partial class Billing
    {
        [JsonProperty("billing_agreement_id")]
        public string BillingAgreementId { get; set; }
    }
}