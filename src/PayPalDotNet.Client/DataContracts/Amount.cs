using System;
using Newtonsoft.Json;

namespace PayPalDotNet.Client.DataContracts
{
    [Serializable]
    public class Amount
    {
        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("total")]
        [JsonConverter(typeof(ParseDecimalStringConverter))]
        public decimal Total { get; set; }

        [JsonProperty("details")]
        public AmountDetails Details { get; set; }

        public Amount()
        {
            this.Details = new AmountDetails();
        }
    }
}
