using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace PayPalDotNet.Client.DataContracts
{
    [Serializable]
    public partial class Sale
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("amount")]
        public Amount Amount { get; set; }

        [JsonProperty("payment_mode")]
        public string PaymentMode { get; set; }

        [JsonProperty("protection_eligibility")]
        public string ProtectionEligibility { get; set; }

        [JsonProperty("protection_eligibility_type")]
        public string ProtectionEligibilityType { get; set; }

        [JsonProperty("transaction_fee")]
        public TransactionAmount TransactionFee { get; set; }

        [JsonProperty("billing_agreement_id")]
        public string BillingAgreementId { get; set; }

        [JsonProperty("parent_payment")]
        public string ParentPayment { get; set; }

        [JsonProperty("create_time")]
        public DateTimeOffset CreateTime { get; set; }

        [JsonProperty("update_time")]
        public DateTimeOffset UpdateTime { get; set; }

        [JsonProperty("links")]
        public IEnumerable<Link> Links { get; set; }

        public Sale()
        {
            this.Amount = new Amount();
            this.TransactionFee = new TransactionAmount();
            this.Links = new List<Link>();
        }
    }
}