﻿using Newtonsoft.Json;
using System;

namespace PayPalDotNet.Client.DataContracts
{
    [Serializable]
    public class PlanUnitList
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("billing_type")]
        public string BillingType { get; set; }
    }
}
