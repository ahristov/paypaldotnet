using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace PayPalDotNet.Client.DataContracts
{
    [Serializable]
    public partial class TransactionFromResponse : Transaction
    {
        [JsonProperty("payee")]
        public Payee Payee { get; set; }

        [JsonProperty("related_resources")]
        public IEnumerable<RelatedResource> RelatedResources { get; set; }

        public TransactionFromResponse() : base()
        {
            this.Payee = new Payee();
            this.RelatedResources = new List<RelatedResource>();
        }
    }
}