using System;
using Newtonsoft.Json;

namespace PayPalDotNet.Client.DataContracts
{
    [Serializable]
    public partial class PayerFromResponse : Payer
    {
        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("payer_info")]
        public PayerInfo PayerInfo { get; set; }

        [JsonIgnore]
        public bool IsVerified => this.Status == "VERIFIED";

        public PayerFromResponse() : base()
        {
            this.PayerInfo = new PayerInfo();
        }
    }
}