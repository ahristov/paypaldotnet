﻿using Newtonsoft.Json;
using System;

namespace PayPalDotNet.Client.DataContracts
{
    [Serializable]
    public class MerchantPreferences
    {
        [JsonProperty("return_url")]
        public string ReturnUrl { get; set; }

        [JsonProperty("cancel_url")]
        public string CancelUrl { get; set; }

        [JsonProperty("notify_url")]
        public string NotifyUrl { get; set; }

        [JsonProperty("accepted_pymt_type")]
        public string AcceptedPymtType { get; set; }

        [JsonProperty("skip_shipping_address")]
        public bool SkipShippingAddress { get; private set; }

        [JsonProperty("immutable_shipping_address")]
        public bool ImmutableShippingAddress { get; private set; }


        public static MerchantPreferences CreateMerchantReferenceForInstantPayments()
        {
            var res = new MerchantPreferences();
            res.ImmutableShippingAddress = true;
            res.SkipShippingAddress = true;
            res.AcceptedPymtType = "INSTANT";
            return res;
        }
    }
}
