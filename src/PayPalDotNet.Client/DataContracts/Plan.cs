﻿using Newtonsoft.Json;
using System;

namespace PayPalDotNet.Client.DataContracts
{
    [Serializable]
    public class Plan
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("merchant_preferences")]
        public MerchantPreferences MerchantPreferences { get; set; }

        public Plan()
        {
            MerchantPreferences = new MerchantPreferences();
        }

        public static Plan CreatePlanForMerchantInitiatedBillingWithInstantPayments()
        {
            var res = new Plan();
            res.Type = "MERCHANT_INITIATED_BILLING";
            res.MerchantPreferences = MerchantPreferences.CreateMerchantReferenceForInstantPayments();
            return res;
        }
    }
}
