using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace PayPalDotNet.Client
{
    public static class Tools
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
            new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
        },
        };

        public static T FromJson<T>(string json) where T : IDto => JsonConvert.DeserializeObject<T>(json, Tools.Settings);
        public static string ToJson<T>(this T self) where T : IDto => JsonConvert.SerializeObject(self, Tools.Settings);

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
            return Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static string UrlEncode(string plainText) => System.Net.WebUtility.UrlEncode(plainText);

        public static HttpRequestMessage CreateHttpRequest(
          HttpMethod method,
          string url,
          IDictionary<string, string> headers)
        {
            var httpRequest = new HttpRequestMessage(method, url);

            if (headers != null)
            {
                foreach (var hdr in headers)
                {
                    var key = (hdr.Key ?? string.Empty).Trim();
                    var val = (hdr.Value ?? string.Empty).Trim();

                    if (!string.IsNullOrWhiteSpace(key))
                    {
                        httpRequest.Headers.Add(key, val);
                    }
                }
            }

            httpRequest.Headers.Add("PayPal-Request-Id", Guid.NewGuid().ToString());

            return httpRequest;
        }

        public static StringContent ToHttpContent(this IDto request) => new StringContent(request.ToJson(), Encoding.UTF8, "application/json");
    }
}