﻿using PayPalDotNet.Client;
using PayPalDotNet.Client.ApiCommands.CreateBillingAgreementToken;
using PayPalDotNet.Client.ApiCommands.PaymentWithReferenceTransaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace PayPalDotNet.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var config = new PayPalClientConfiguration();
            using (HttpClient httpClient = new HttpClient())
            {
                IPayPalClient client = new PayPalClient(config, httpClient);

                // Create billing agreement token
                // --------------------------------------------------

                var baToken = client.CreateBillingAgreementToken(
                    new CreateBillingAgreementTokenRequest("https://example.com/return", "https://example.com/cancel")
                );

                string approval_url = baToken.ApprovalUrl;
                string ba_token = baToken.TokenId;

                Console.WriteLine($@"
Billing agreement token ID created.
Needs approval from the user.
The user gets redirected to the below approval_url.
---
ba_token={ba_token}
approval_url={approval_url}
");


                // User redirect to PayPal to approve agreement
                // --------------------------------------------------

                Prompt($"Open in the browser: {approval_url}", "Login as user, approve the token, then come back here.");


                // Show billing agreement token
                // --------------------------------------------------

                var baTokenDtl = client.ShowBillingAgreementToken(ba_token);
                if (!baTokenDtl.IsApproved)
                {
                    Prompt("Billing agreement is not approved!", "Exit.");
                    return;
                }

                Console.WriteLine($@"
Show billing agreement - billing agreement is approved.
---
ba_token={ba_token}
    token_status={baTokenDtl.TokenStatus}
    payer_info.email={baTokenDtl.PayerInfo.Email}
    payer_info.first_name={baTokenDtl.PayerInfo.FirstName}
    payer_info.last_name={baTokenDtl.PayerInfo.LastName}
    payer_info.payer_id={baTokenDtl.PayerInfo.PayerId}
    payer_info.country_code={baTokenDtl.PayerInfo.CountryCode}
    owner.merchant_id={baTokenDtl.Owner.MerchantId}
    owner.email={baTokenDtl.Owner.Email}
");


                // Create billing agreement
                // --------------------------------------------------

                var billingAgreement = client.CreateBillingAgreement(baTokenDtl.TokenId);
                if (!billingAgreement.IsActive)
                {
                    Console.WriteLine($"Billing agreement not created or not active!", "Exit.");
                }

                string billing_agreement = billingAgreement.Id;

                Console.WriteLine($@"
Billing agreement created.
---
billing_agreement={billing_agreement}
    state={billingAgreement.State}
    payer.payer_info.email={billingAgreement.Payer?.PayerInfo?.Email}
    payer.payer_info.first_name={billingAgreement.Payer?.PayerInfo?.FirstName}
    payer.payer_info.last_name={billingAgreement.Payer?.PayerInfo?.LastName}
    payer.payer_info.payer_id={billingAgreement.Payer?.PayerInfo?.PayerId}
    payer.payer_info.country_code={billingAgreement.Payer?.PayerInfo?.CountryCode}
    merchant.payee_info.email={billingAgreement.Merchant.PayeeInfo.Email}
");


                // Show billing agreement
                // --------------------------------------------------

                var billingAgreementDtl = client.ShowBillingAgreement(billing_agreement);
                if (!billingAgreementDtl.IsActive)
                {
                    Console.WriteLine($"Billing agreement is not active!", "Exit.");
                }

                Console.WriteLine($@"
Show billing agreement - billing agreement is active.
---
billing_agreement={billing_agreement}
    state={billingAgreementDtl.State}
    payer.payer_info.email={billingAgreementDtl.Payer?.PayerInfo?.Email}
    payer.payer_info.first_name={billingAgreementDtl.Payer?.PayerInfo?.FirstName}
    payer.payer_info.last_name={billingAgreementDtl.Payer?.PayerInfo?.LastName}
    payer.payer_info.payer_id={billingAgreementDtl.Payer?.PayerInfo?.PayerId}
    payer.payer_info.country_code={billingAgreementDtl.Payer?.PayerInfo?.CountryCode}
    merchant.payee_info.email={billingAgreementDtl.Merchant.PayeeInfo.Email}
");

                // Payment processing with reference transction 
                // --------------------------------------------------

                var rand = new Random();
                int userId = rand.Next();
                int trxId = rand.Next();
                string invoiceNumber = $"{userId}-{trxId}";

                var payment = client.PaymentWithReferenceTransaction(new PaymentWithReferenceTransactionRequest
                {
                    Intent = "sale",
                    Payer = new Client.DataContracts.Payer
                    {
                        PaymentMethod = "PAYPAL",
                        FundingInstruments = new List<Client.DataContracts.FundingInstrument>()
                        {
                            new Client.DataContracts.FundingInstrument
                            {
                            Billing = new Client.DataContracts.Billing
                            {
                                BillingAgreementId = billingAgreement.Id
                            }
                            }
                        }
                    },
                    Transactions = new List<Client.DataContracts.Transaction>
                    {
                        new Client.DataContracts.Transaction
                        {
                            Amount = new Client.DataContracts.Amount
                            {
                                Currency = "USD",
                                Total = 106.60m, // Incl.Tax
                                Details = new Client.DataContracts.AmountDetails
                                {
                                    Subtotal = 100.00m,
                                    Tax = 6.600m,
                                },
                            },
                            Description = "1 Month Match Subscription",
                            Custom = "",
                            NoteToPayee = "Automatic renew",
                            InvoiceNumber = invoiceNumber.ToString(),
                        }
                    },
                    RedirectUrls = new Client.DataContracts.RedirectUrls
                    {
                        ReturnUrl = "https://example.com/return",
                        CancelUrl = "https://example.com/calcel"
                    },
                });

                if (!payment.IsApproved)
                {
                    Console.WriteLine($"Payment is not approved!", "Exit.");
                    return;
                }

                string sale_id = payment.SaleId;

                Console.WriteLine($@"
Payment is approved.
---
sale_id={sale_id}
    state={payment.State}
    intent={payment.Intent}
    payer.payer_info.email={payment.Payer?.PayerInfo?.Email}
    payer.payer_info.first_name={payment.Payer?.PayerInfo?.FirstName}
    payer.payer_info.last_name={payment.Payer?.PayerInfo?.LastName}
    payer.payer_info.payer_id={payment.Payer?.PayerInfo?.PayerId}
    payer.payer_info.country_code={payment.Payer?.PayerInfo?.CountryCode}
    transactions[0].description={payment.Transactions?.FirstOrDefault()?.Description}
    transactions[0].invoice_number={payment.Transactions?.FirstOrDefault()?.InvoiceNumber}
    transactions[0].amount.total={payment.Transactions?.FirstOrDefault()?.Amount.Total:F2}
    transactions[0].amount.currency={payment.Transactions?.FirstOrDefault()?.Amount.Currency}
    transactions[0].amount.details.subtotal={payment.Transactions?.FirstOrDefault()?.Amount?.Details?.Subtotal:F2}
    transactions[0].amount.currency={payment.Transactions?.FirstOrDefault()?.Amount?.Details?.Tax:F2}
    transactions[0].payee.merchant_id={payment.Transactions?.FirstOrDefault()?.Payee?.MerchantId}
    transactions[0].payee.email={payment.Transactions?.FirstOrDefault()?.Payee?.Email}
");


                // Show sale
                // --------------------------------------------------

                var saleDtl = client.ShowSale(sale_id);
                if (!saleDtl.IsCompleted)
                {
                    Console.WriteLine($"Sale {sale_id} not completed!", "Exit.");
                }

                Console.WriteLine($@"
Show sale - sale is completed.
---
sale_id={sale_id}
    state={saleDtl.State}
    invoice_number={saleDtl.InvoiceNumber}
    amount.total={saleDtl.Amount?.Total:F2}
    amount.currency={saleDtl.Amount?.Currency}
    transaction_fee.value={saleDtl.TransactionFee?.Value:F2}
    transaction_fee.currency={saleDtl.TransactionFee?.Currency}
");


                // Refund half of the sale
                // --------------------------------------------------

                int refundTrxId = rand.Next();
                string refundInvoiceNumber = $"{invoiceNumber}-{refundTrxId}";

                var refund = client.RefundSale(sale_id, new Client.ApiCommands.RefundSale.RefundSaleRequest
                {
                    InvoiceNumber = refundInvoiceNumber,
                    Amount = new Client.DataContracts.Amount
                    {
                        Total = 53.30m, // Incl.Tax
                        Currency = "USD",
                    }
                });
                if (!refund.IsCompleted)
                {
                    Console.WriteLine($"Refund No {refundInvoiceNumber} not completed!", "Exit.");
                }

                string refund_id = refund.Id;

                Console.WriteLine($@"
Refund sale - refund is completed.
---
refund_id={refund_id}
    state={refund.State}
    amount.total={refund.Amount?.Total:F2}
    amount.currency={refund.Amount?.Currency:F2}
    refund_from_transaction_fee.value={refund.RefundFromTransactionFee?.Value:F2}
    refund_from_transaction_fee.currency={refund.RefundFromTransactionFee?.Currency}
    refund_from_received_amount.value={refund.RefundFromReceivedAmount?.Value:F2}
    refund_from_received_amount.currency={refund.RefundFromReceivedAmount?.Currency}
    total_refunded_amount.value={refund.TotalRefundedAmount?.Value:F2}
    total_refunded_amount.currency={refund.TotalRefundedAmount?.Currency}
");



                // Show first partial refund
                // --------------------------------------------------

                var refundDtl = client.ShowRefund(refund_id);
                if (!saleDtl.IsCompleted)
                {
                    Console.WriteLine($"Refund {refund_id} not completed!", "Exit.");
                }

                Console.WriteLine($@"
Show refund - first refund is completed.
---
refund_id={refund_id}
    state={refundDtl.State}
    amount.total={refundDtl.Amount?.Total:F2}
    amount.currency={refundDtl.Amount?.Currency:F2}
    refund_from_transaction_fee.value={refundDtl.RefundFromTransactionFee?.Value:F2}
    refund_from_transaction_fee.currency={refundDtl.RefundFromTransactionFee?.Currency}
    refund_from_received_amount.value={refundDtl.RefundFromReceivedAmount?.Value:F2}
    refund_from_received_amount.currency={refundDtl.RefundFromReceivedAmount?.Currency}
    total_refunded_amount.value={refundDtl.TotalRefundedAmount?.Value:F2}
    total_refunded_amount.currency={refundDtl.TotalRefundedAmount?.Currency}
");



                // Show sale after partial refund
                // --------------------------------------------------

                var saleDtlAfterRefund = client.ShowSale(sale_id);
                if (!saleDtlAfterRefund.IsPartiallyRefunded)
                {
                    Console.WriteLine($"Sale {sale_id} not partially refunded!", "Exit.");
                }

                Console.WriteLine($@"
Show sale after partial refund - sale is partially refunded.
---
sale_id={sale_id}
    state={saleDtlAfterRefund.State}
    invoice_number={saleDtlAfterRefund.InvoiceNumber}
    amount.total={saleDtlAfterRefund.Amount?.Total:F2}
    amount.currency={saleDtlAfterRefund.Amount?.Currency}
    amount.details.subtotal={saleDtlAfterRefund.Amount?.Details?.Subtotal:F2}
    amount.details.tax={saleDtlAfterRefund.Amount?.Details?.Tax:F2}
    transaction_fee.value={saleDtlAfterRefund.TransactionFee?.Value:F2}
    transaction_fee.currency={saleDtlAfterRefund.TransactionFee?.Currency}
");


                // Refund second half of the sale
                // --------------------------------------------------

                int secondRefundTrxId = rand.Next();
                string secondRefundInvoiceNumber = $"{invoiceNumber}-{secondRefundTrxId}";

                var secondRefund = client.RefundSale(sale_id, new Client.ApiCommands.RefundSale.RefundSaleRequest
                {
                    InvoiceNumber = secondRefundInvoiceNumber,
                    Amount = new Client.DataContracts.Amount
                    {
                        Total = 53.30m, // Incl.Tax
                        Currency = "USD",
                    }
                });
                if (!secondRefund.IsCompleted)
                {
                    Console.WriteLine($"Second refund No {secondRefundInvoiceNumber} not completed!", "Exit.");
                }

                string second_refund_id = secondRefund.Id;

                Console.WriteLine($@"
Refund sale - second refund is completed.
---
refund_id={second_refund_id}
    state={secondRefund.State}
    amount.total={secondRefund.Amount?.Total:F2}
    amount.currency={secondRefund.Amount?.Currency:F2}
    refund_from_transaction_fee.value={secondRefund.RefundFromTransactionFee?.Value:F2}
    refund_from_transaction_fee.currency={secondRefund.RefundFromTransactionFee?.Currency}
    refund_from_received_amount.value={secondRefund.RefundFromReceivedAmount?.Value:F2}
    refund_from_received_amount.currency={secondRefund.RefundFromReceivedAmount?.Currency}
    total_refunded_amount.value={secondRefund.TotalRefundedAmount?.Value:F2}
    total_refunded_amount.currency={secondRefund.TotalRefundedAmount?.Currency}
");



                // Show second partial refund
                // --------------------------------------------------

                var secondRefundDtl = client.ShowRefund(second_refund_id);
                if (!secondRefundDtl.IsCompleted)
                {
                    Console.WriteLine($"Second refund {second_refund_id} is not completed!", "Exit.");
                }

                Console.WriteLine($@"
Show refund - second refund is completed.
---
refund_id={second_refund_id}
    state={secondRefundDtl.State}
    amount.total={secondRefundDtl.Amount?.Total:F2}
    amount.currency={secondRefundDtl.Amount?.Currency:F2}
    refund_from_transaction_fee.value={secondRefundDtl.RefundFromTransactionFee?.Value:F2}
    refund_from_transaction_fee.currency={secondRefundDtl.RefundFromTransactionFee?.Currency}
    refund_from_received_amount.value={secondRefundDtl.RefundFromReceivedAmount?.Value:F2}
    refund_from_received_amount.currency={secondRefundDtl.RefundFromReceivedAmount?.Currency}
    total_refunded_amount.value={secondRefundDtl.TotalRefundedAmount?.Value:F2}
    total_refunded_amount.currency={secondRefundDtl.TotalRefundedAmount?.Currency}
");


                // Show sale after full refund
                // --------------------------------------------------

                var saleDtlAfterSecondRefund = client.ShowSale(sale_id);
                if (!saleDtlAfterSecondRefund.IsFullyRefunded)
                {
                    Console.WriteLine($"Sale {sale_id} not fully refunded!", "Exit.");
                }

                Console.WriteLine($@"
Show sale after full refund - sale is fully refunded.
---
sale_id={sale_id}
    state={saleDtlAfterSecondRefund.State}
    invoice_number={saleDtlAfterSecondRefund.InvoiceNumber}
    amount.total={saleDtlAfterSecondRefund.Amount?.Total:F2}
    amount.currency={saleDtlAfterSecondRefund.Amount?.Currency}
    amount.details.subtotal={saleDtlAfterSecondRefund.Amount?.Details?.Subtotal:F2}
    amount.details.tax={saleDtlAfterSecondRefund.Amount?.Details?.Tax:F2}
    transaction_fee.value={saleDtlAfterSecondRefund.TransactionFee?.Value:F2}
    transaction_fee.currency={saleDtlAfterSecondRefund.TransactionFee?.Currency}
");


                // Cancel billing agreement
                // --------------------------------------------------
                client.CancelBillingAgreement(billing_agreement);


                // Show billing agreement after cancellation
                // --------------------------------------------------

                var billingAgreementDtlAfterCancellation = client.ShowBillingAgreement(billing_agreement);
                if (!billingAgreementDtlAfterCancellation.IsInactive)
                {
                    Console.WriteLine($"Billing agreement is not inactive!", "Exit.");
                }

                Console.WriteLine($@"
Show billing agreement after cancellation - billing agreement is inactive.
---
billing_agreement={billing_agreement}
    state={billingAgreementDtlAfterCancellation.State}
    payer.payer_info.email={billingAgreementDtlAfterCancellation.Payer?.PayerInfo?.Email}
    payer.payer_info.first_name={billingAgreementDtlAfterCancellation.Payer?.PayerInfo?.FirstName}
    payer.payer_info.last_name={billingAgreementDtlAfterCancellation.Payer?.PayerInfo?.LastName}
    payer.payer_info.payer_id={billingAgreementDtlAfterCancellation.Payer?.PayerInfo?.PayerId}
    payer.payer_info.country_code={billingAgreementDtlAfterCancellation.Payer?.PayerInfo?.CountryCode}
    merchant.payee_info.email={billingAgreementDtlAfterCancellation.Merchant.PayeeInfo.Email}
");



                Prompt("End.");
                //End.
            }
        }


        private static void Prompt(params string[] messages)
        {
            foreach (var msg in messages)
            {
                Console.WriteLine(msg);
            }

            Console.WriteLine("Press [Enter] to continue.");
            Console.ReadLine();
        }
    }
}
