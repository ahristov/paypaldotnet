# PayPal SDK for Dot.Net

This project contains SDK to connect to [PayPal REST API](https://developer.paypal.com/docs/api/overview/)

## ðŸ  [Homepage](https://bitbucket.org/ahristov/paypaldotnet)

## Usage

```sh
cd src
dotnet build
dotnet run -p PayPalDotNet.ConsoleApp
```

## Author

ðŸ‘¤ **Atanas Hristov**

### Links

[PayPal REST API](https://developer.paypal.com/docs/api/overview/)
